import {EntryDetail} from './entry_detail.model';
export class Entry{
  public id:number;
  public supplier_name:string;
  public supplier_id:number;
  public entry_type:string;
  public datetime:string;
  public description:string;
  public total:number;
  public status:string;
  public comment:string;
  public entry_details:EntryDetail[];

  public constructor(id:number,supplier_name:string,supplier_id:number,
  entry_type:string,datetime:string,description:string,
  total:number,status:string,comment:string){
    this.id=id;
    this.supplier_name=supplier_name;
    this.supplier_id=supplier_id;
    this.entry_type=entry_type;
    this.datetime=datetime;
    this.description=description;
    this.total=total;
    this.status=status;
    this.comment=comment;
    this.entry_details=null;
  }
}
