export class Product{
  constructor(
    public id:number,
    public name:string,
    public code:string,
    public description:string,
    public category_id:number,
    public supplier_id:number,
    public cost_price:number,
    public sale_price:number,
    public category_name?:string,
    public supplier_name?:string
  ){}
}
