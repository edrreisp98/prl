export class EntryDetail{
  public id:number;
  public product_name:string;
  public product_code:string;
  public product_id:number;
  public quantity:number;
  public cost_price:number;
  public sale_price:number;

  public constructor(id:number,product_name:string,product_code:string,
  product_id:number,quantity:number,cost_price:number,sale_price:number){
    this.id=id;
    this.product_name=product_name;
    this.product_code=product_code;
    this.product_id=product_id;
    this.quantity=quantity;
    this.cost_price=cost_price;
    this.sale_price=sale_price;
  }
}
