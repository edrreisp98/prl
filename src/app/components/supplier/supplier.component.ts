import { Component, OnInit } from '@angular/core';
import {Supplier} from '../../models/supplier.model';
import {SuppliersService} from '../../services/suppliers.service';
declare var $,swal;

@Component({
  selector: 'app-suppliers',
  templateUrl: './supplier.component.html'
})
export class SupplierComponent implements OnInit {
  public suppliers:Supplier[]=[];
  public supplier:Supplier;
  public creando:boolean=false;
  public editando:boolean=false;
  public textoBoton:string;
  public loading:boolean=false;
  constructor(private suppliersService:SuppliersService) {
    this.supplier=new Supplier(0,'','');
    this.textoBoton="Crear suplidor";
  }
  //Metodo para validar el objeto
  validateModel=(supplier:Supplier):boolean=>{
    return supplier.name!="" ;
  }
  editarSuplidor=(supplier:Supplier)=>{
    this.creando=false;
    this.editando=true;
    this.textoBoton="Cancelar edición";
    this.supplier=supplier;
  }
  botones=()=>{
    if(!this.creando && !this.editando){
      this.creando=true;
      this.supplier=new Supplier(0,'','');
      this.textoBoton="Cancelar";
    }else if(this.creando){
      this.creando=false;
      this.supplier=new Supplier(0,'','');
      this.textoBoton="Crear categoría";
    }else if(this.editando){
      this.editando=false;
      this.supplier=new Supplier(0,'','');
      this.textoBoton="Crear categoría";
    }
  }


  onSubmit=()=>{
    if(this.creando){
      this.suppliersService.addSupplier(this.supplier)
      .subscribe(
        result=>{
          if(result.code==200){
            swal(
              'Éxito',
              'Suplidor agregado',
              'success'
            );
            this.getSuppliers();
          }else{
            swal(
              'Suplidor no agregado',
              'Algo salió mal!',
              'error'
            );
          }
          this.creando=false;
          this.textoBoton="Crear suplidor";
        },
        error=>{
          console.error(error);
        }
      );
    }else if(this.editando){
      this.suppliersService.editSupplier(this.supplier,this.supplier.id)
      .subscribe(
        result=>{
          if(result.code==200){
            swal(
              'Éxito',
              'Suplidor modificado',
              'success'
            );
            this.getSuppliers();
          }else{
            swal(
              'Suplidor no modificado',
              'Algo salió mal!',
              'error'
            );
          }
          this.editando=false;
          this.textoBoton="Crear suplidor";
        },
        error=>{
          console.error(error);
        }
      );
    }

  }
  getSuppliers=()=>{
    this.loading=true;
    this.suppliersService.getSuppliers()
    .subscribe(
      result=>{
        if(result.code==200){
          this.suppliers=result.data;
        }
      },
      error=>{
        console.error(error);
      },
      ()=>{
        this.loading=false;
      }
    );
  }


  deleteSuplidor=(supplierid:number)=>{
    this.suppliersService.deleteSupplier(supplierid).subscribe(response=>{
      if(response.code==200){
        swal(
          'Éxito',
          'Suplidor eliminado',
          'success'
        );
        this.getSuppliers();
      }else{
        swal(
          'Suplidor no eliminado',
          'Algo salió mal!',
          'error'
        );
      }
    });
  }
  confirmarBorrarSuplidor=(supplier:Supplier)=>{
    swal({title: 'Eliminar suplidor', text: `Estás seguro de eliminar el suplidor ${supplier.name}?`,
          type: 'warning',showCancelButton: true,confirmButtonColor: '#3085d6',cancelButtonColor: '#d33',confirmButtonText: 'Si!',
          cancelButtonText: 'No',
        }).then(() =>{
          this.deleteSuplidor(supplier.id);
        },()=>{
    });
  }


  ngOnInit() {
    this.getSuppliers();
  }

}
