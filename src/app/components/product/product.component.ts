import { Component, OnInit } from '@angular/core';
import {Category} from '../../models/category.model';
import {CategoriesService} from '../../services/categories.service';
import {Supplier} from '../../models/supplier.model';
import {SuppliersService} from '../../services/suppliers.service';
import {ProductsService} from '../../services/products.service';
import {Product} from '../../models/product.model';
declare var $,swal;
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html'
})
export class ProductComponent implements OnInit {
  public categories:Category[]=[];
  public suppliers:Supplier[]=[];

  public products:Product[]=[];
  public product:Product;

  public creando:boolean=false;
  public editando:boolean=false;
  public textoBoton:string;
  public loading:boolean=false;
  constructor(
    private categoriesService:CategoriesService,
    private suppliersService:SuppliersService,
    private productsService:ProductsService
  ) {
    this.product=new Product(0,'','','',0,0,0,0);
    this.textoBoton="Crear producto";
  }
  //Metodo para validar el objeto
  validateModel=(product:Product):boolean=>{
    return product.name!="" && product.category_id!=0 && product.supplier_id!=0
    && product.cost_price>0 && product.sale_price>0;
  }
  editarProducto=(product:Product)=>{
    this.creando=false;
    this.editando=true;
    this.textoBoton="Cancelar edición";
    this.product=product;
  }
  onSubmit=()=>{
    if(this.creando){
      this.productsService.addProduct(this.product)
      .subscribe(
        result=>{
          if(result.code==200){
            swal(
              'Éxito',
              'Producto agregado',
              'success'
            );
            this.getProducts();
          }else{
            swal(
              'Producto no agregado',
              'Algo salió mal!',
              'error'
            );
          }
          this.creando=false;
          this.textoBoton="Crear producto";
        },
        error=>{
          console.error(error);
        }
      );
    }else if(this.editando){
      this.productsService.editProduct(this.product,this.product.id)
      .subscribe(
        result=>{
          if(result.code==200){
            swal(
              'Éxito',
              'Producto modificado',
              'success'
            );
            this.getProducts();
          }else{
            swal(
              'Producto no modificado',
              'Algo salió mal!',
              'error'
            );
          }
          this.editando=false;
          this.textoBoton="Crear producto";
        },
        error=>{
          console.error(error);
        }
      );
    }
  };
  getProducts=()=>{
    this.loading=true;
    this.productsService.getProducts()
    .subscribe(
      result=>{
        if(result.code==200){
          this.products=result.data;
        }
      },
      error=>{
        console.error(error);
      },
      ()=>{
        this.loading=false;
      }
    );
  }
  loadCategories=()=>{
    this.categoriesService.getCategories().
    subscribe(
      result=>{
        if(result.code==200){
          this.categories=result.data;
        }else{
          console.log(result);
        }
      },
      error=>{console.error(error);}
    );
  }
  deleteProducto=(productid:number)=>{
    this.productsService.deleteProduct(productid).subscribe(response=>{
      if(response.code==200){
        swal(
          'Éxito',
          'Producto eliminado',
          'success'
        );
        this.getProducts();
      }else{
        swal(
          'Producto no eliminado',
          'Algo salió mal!',
          'error'
        );
      }
    });
  }
  confirmarBorrarProducto=(product:Product)=>{
    swal({title: 'Eliminar producto', text: `Estás seguro de eliminar el producto ${product.name}?`,
          type: 'warning',showCancelButton: true,confirmButtonColor: '#3085d6',cancelButtonColor: '#d33',confirmButtonText: 'Si!',
          cancelButtonText: 'No',
        }).then(() =>{
          this.deleteProducto(product.id);
        },()=>{
    });
  }
  loadSuppliers=()=>{
    this.suppliersService.getSuppliers().
    subscribe(
      result=>{
        if(result.code==200){
          this.suppliers=result.data;
        }else{
          console.log(result);
        }
      },
      error=>{console.error(error);}
    );
  }
  botones=()=>{
    if(!this.creando && !this.editando){
      this.creando=true;
      this.product=new Product(0,'','','',0,0,0,0);
      this.textoBoton="Cancelar";
    }else if(this.creando){
      this.creando=false;
      this.product=new Product(0,'','','',0,0,0,0);
      this.textoBoton="Crear producto";
    }else if(this.editando){
      this.editando=false;
      this.product=new Product(0,'','','',0,0,0,0);
      this.textoBoton="Crear producto";
    }
  }
  ngOnInit() {
    this.loadSuppliers();
    this.loadCategories();
    this.getProducts();
  }

}
