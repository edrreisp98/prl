import { Component, OnInit } from '@angular/core';
import {Entry} from '../../models/entry.model';
import {EntryDetail} from '../../models/entry_detail.model';
import {EntryService} from '../../services/entry.service';
import {Supplier} from '../../models/supplier.model';
import {SuppliersService} from '../../services/suppliers.service';

import {Product} from '../../models/product.model';
import {ProductsService} from '../../services/products.service';
declare var alertify,swal;
@Component({
  selector: 'app-entry',
  templateUrl: './entry.component.html',
  styles: []
})
export class EntryComponent implements OnInit {
  public loading:boolean=false;
  public creando:boolean=false;
  public mostrando:boolean=false;
  public suppliers:Supplier[]=[];
  public products:Product[]=[];
  public entries:Entry[]=[];



  public entry:Entry;
  public entry_details:EntryDetail[]=[];

  constructor(private supplierService:SuppliersService,
    private productsService:ProductsService,
    private entryService:EntryService) {
    this.InicializarEntry();
    this.getSuppliers();
    this.getProducts();
  }
  verIngreso=(entryEntrante:Entry)=>{
    this.mostrando=true;
    this.creando=false;
    this.entry=entryEntrante;
    this.entry.entry_details=entryEntrante.entry_details;
  };
  cancelEntry=(entryEntrante:Entry)=>{
    this.entryService.cancelEntry(entryEntrante).subscribe(response=>{
      if(response.code==200){
        alertify.success('Ingreso anulado');    
        this.getEntries();
      }else if(response.code==300){
        alertify.error('Este ingreso ya ha sido anulado');
      }else{
        alertify.error('Ingreso no anulado');
      }
    });
  };
  cancelEntryConfirm=(entryEntrante:Entry)=>{
    swal({title: 'Estás seguro de anular el ingreso?', text: `Los productos se reducirán del stock`,
          type: 'warning',showCancelButton: true,confirmButtonColor: '#3085d6',cancelButtonColor: '#d33',confirmButtonText: 'Si!',
          cancelButtonText: 'No',
        }).then(() =>{
          this.cancelEntry(entryEntrante);
        },()=>{
    });
  };
  addProductToList=(product:Product)=>{
    if(this.productExistsInList(product)){
      alertify.message('El producto ya se encuentra en la lista');
      return;
    }
    //SI llego a este punto, quiere decir que el producto no ha sido añadido a la lista entonces lo agrego
    this.entry.entry_details.push(
      new EntryDetail(0,product.name,product.code,product.id,1,product.cost_price,product.sale_price)
    );
  };
  onSubmit=()=>{
    if(this.creando){
      this.entryService.addEntry(this.entry)
      .subscribe(
        result=>{
          if(result.code==200){
            swal(
              'Éxito',
              'Ingreso aprovisionado',
              'success'
            );
            this.getEntries();
          }else{
            swal(
              'Ingreso no realizado',
              'Algo salió mal!',
              'error'
            );
          }
        }
      );
      this.creando=false;
      this.InicializarEntry();
    }
  };
  TotalizarDetails=():number=>{
    let total_:number=0;
    if(this.entry.entry_details){
      for(var i=0;i<this.entry.entry_details.length;i++){
        total_+=(this.entry.entry_details[i].cost_price*this.entry.entry_details[i].quantity);
      }
    }
    this.entry.total=total_;
    return total_;
  }
  removeProduct=(IndexentryDetailToRemove:number)=>{
    this.entry.entry_details.splice(IndexentryDetailToRemove,1);
  };
  productExistsInList=(producto:Product):boolean=>{
    if(this.entry.entry_details){
      for (var i = 0; i < this.entry.entry_details.length; i++) {
          if (this.entry.entry_details[i].product_id === producto.id) {
              return true;
          }
      }
    }
    return false;
  };
  getProducts=()=>{
    this.productsService.getProducts().subscribe(
      result=>{
        if(result.code==200){
          this.products=result.data;
        }
      }, error=>{
        console.error(error);
      }
    );
  }
  getSuppliers=()=>{
    this.supplierService.getSuppliers().subscribe(
      result=>{
        if(result.code==200){
          this.suppliers=result.data;
        }
      }, error=>{
        console.error(error);
      }
    );
  };
  cancelar=()=>{
    this.mostrando=false;
    this.creando=false;
    this.InicializarEntry();
  };
  nuevo=()=>{
    this.creando=true;
    this.InicializarEntry();
  }
  InicializarEntry=()=>{
    //INICIALIZO el ENTRY
    this.entry=new Entry(0,'',0,'entry','','',0,'EMITIDO','');
    //INICIALIZO SUS ENTRY_DETAILS
    this.entry.entry_details=this.entry_details.splice(0,this.entry_details.length);
  };
  validate=(entry:Entry)=>{
    return entry.supplier_id!=0 &&
           entry.supplier_name!="" &&
           entry.entry_type!="" &&
           entry.status!="" &&
           (entry.entry_details!=null && entry.entry_details.length>0);
  };
  getEntries=()=>{
    this.loading=true;
    this.entryService.getEntries().subscribe(
      result=>{
        if(result.code==200){
          this.entries=result.data;
        }
      },
      error=>{
        console.error(error);
      },
      ()=>{
        this.loading=false;
      }
    );
  }
  ngOnInit() {
    this.getEntries();
  }

}
