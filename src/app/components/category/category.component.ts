import { Component, OnInit,AfterViewInit } from '@angular/core';
import {Category} from '../../models/category.model';
import {CategoriesService} from '../../services/categories.service';

declare var $,swal;

@Component({
  selector: 'app-categoria',
  templateUrl: './category.component.html',
  styles: []
})
export class CategoryComponent implements OnInit,AfterViewInit {
  public categories:Category[]=[];
  public category:Category;
  public creando:boolean=false;
  public editando:boolean=false;
  public textoBoton:string;
  public loading:boolean=false;
  constructor(private categoriesService:CategoriesService) {
    this.category=new Category(0,'','');
    this.textoBoton="Crear categoría";
  }
  //Metodo para validar el objeto
  validateModel=(category:Category):boolean=>{
    return category.name!="" ;
  }
  editarCategoria=(category:Category)=>{
    this.creando=false;
    this.editando=true;
    this.textoBoton="Cancelar edición";
    this.category=category;
  }
  botones=()=>{
    if(!this.creando && !this.editando){
      this.creando=true;
      this.category=new Category(0,'','');
      this.textoBoton="Cancelar";
    }else if(this.creando){
      this.creando=false;
      this.category=new Category(0,'','');
      this.textoBoton="Crear categoría";
    }else if(this.editando){
      this.editando=false;
      this.category=new Category(0,'','');
      this.textoBoton="Crear categoría";
    }
  }
  onSubmit=()=>{
    if(this.creando){
      this.categoriesService.addCategory(this.category)
      .subscribe(
        result=>{
          if(result.code==200){
            swal(
              'Éxito',
              'Categoría agregada',
              'success'
            );
            this.getCategorias();
          }else{
            swal(
              'Categoría no agregada',
              'Algo salió mal!',
              'error'
            );
          }
          this.creando=false;
          this.textoBoton="Crear categoría";
        },
        error=>{
          console.error(error);
        }
      );
    }else if(this.editando){
      this.categoriesService.editCategory(this.category,this.category.id)
      .subscribe(
        result=>{
          if(result.code==200){
            swal(
              'Éxito',
              'Categoría modificada',
              'success'
            );
            this.getCategorias();
          }else{
            swal(
              'Categoría no modificada',
              'Algo salió mal!',
              'error'
            );
          }
          this.editando=false;
          this.textoBoton="Crear categoría";
        },
        error=>{
          console.error(error);
        }
      );
    }

  }
  getCategorias=()=>{
    this.loading=true;
    this.categoriesService.getCategories()
    .subscribe(
      result=>{
        if(result.code==200){
          this.categories=result.data;
        }
      },
      error=>{
        console.error(error);
      },
      ()=>{
        this.loading=false;
      }
    );
  }

  ngOnInit() {
    this.getCategorias();
  }
  ngAfterViewInit(){
    /*
    setTimeout(()=>{
      this.tabla=$('#tableCategorias').DataTable();
    },1000);
    */
  }

  deleteCategoria=(categoryid:number)=>{
    this.categoriesService.deleteCategory(categoryid).subscribe(response=>{
      if(response.code==200){
        swal(
          'Éxito',
          'Categoría eliminada',
          'success'
        );
        this.getCategorias();
      }else{
        swal(
          'Categoría no eliminada',
          'Algo salió mal!',
          'error'
        );
      }
    });
  }
  confirmarBorrarCategoria=(category:Category)=>{
    swal({title: 'Eliminar categoría', text: `Estás seguro de eliminar la categoría ${category.name}?`,
          type: 'warning',showCancelButton: true,confirmButtonColor: '#3085d6',cancelButtonColor: '#d33',confirmButtonText: 'Si!',
          cancelButtonText: 'No',
        }).then(() =>{
          this.deleteCategoria(category.id);
        },()=>{
    });
  }
}
