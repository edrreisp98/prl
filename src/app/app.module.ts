import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { CategoryComponent } from './components/category/category.component';
import { SupplierComponent } from './components/supplier/supplier.component';
import { ProductComponent } from './components/product/product.component';
import { EntryComponent } from './components/entry/entry.component';


import {APP_ROUTING} from './app.routes';



//SERVICIOS
import {CategoriesService} from './services/categories.service';
import {SuppliersService} from './services/suppliers.service';
import {ProductsService} from './services/products.service';
import {EntryService} from './services/entry.service';
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    CategoryComponent,
    SupplierComponent,
    ProductComponent,
    EntryComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    APP_ROUTING
  ],
  providers: [
    CategoriesService,
    SuppliersService,
    ProductsService,
    EntryService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
