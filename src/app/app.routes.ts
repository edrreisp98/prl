import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {CategoryComponent} from './components/category/category.component';
import {SupplierComponent} from './components/supplier/supplier.component';
import {ProductComponent} from './components/product/product.component';
import {EntryComponent} from './components/entry/entry.component';

const APP_ROUTES: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'category', component: CategoryComponent },
  { path: 'supplier', component: SupplierComponent },
  { path: 'product', component: ProductComponent },
  { path: 'entry', component: EntryComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'inicio' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
