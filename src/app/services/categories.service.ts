import { Injectable } from '@angular/core';
import {GLOBAL} from './global';
import {Category} from '../models/category.model';
import {Http,Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class CategoriesService {
  private url:string;
  constructor(private http:Http) {
    this.url=GLOBAL.url;
  }
  getCategories=()=>{
    return this.http.get(this.url+"categories")
    .map(res=>res.json());
  };
  getCategory=(id:number)=>{
    return this.http.get(this.url+`category/${id}`)
    .map(res=>res.json());
  };
  addCategory=(category:Category)=>{
    let json:string=JSON.stringify(category);
    let params=`json=${json}`;
    let headers=new Headers({'Content-Type':'application/x-www-form-urlencoded'});

    return this.http.post(this.url+"categories",params,{headers:headers})
    .map(res=>res.json());
  };
  editCategory=(category:Category,id:number)=>{
    let json:string=JSON.stringify(category);
    let params=`json=${json}`;
    let headers=new Headers({'Content-Type':'application/x-www-form-urlencoded'});

    return this.http.post(this.url+"update-category/"+id,params,{headers:headers})
    .map(res=>res.json());
  };
  deleteCategory=(id:number)=>{
    return this.http.get(this.url+`delete-category/${id}`)
    .map(res=>res.json());
  };
}
