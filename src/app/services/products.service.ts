import { Injectable } from '@angular/core';
import {GLOBAL} from './global';
import {Product} from '../models/product.model';
import {Http,Headers} from '@angular/http';
@Injectable()
export class ProductsService {

  private url:string;
  constructor(private http:Http) {
    this.url=GLOBAL.url;
  }

  getProducts=()=>{
    return this.http.get(this.url+"products")
    .map(res=>res.json());
  };
  getProduct=(id:number)=>{
    return this.http.get(this.url+`product/${id}`)
    .map(res=>res.json());
  };
  addProduct=(product:Product)=>{
    let json:string=JSON.stringify(product);
    let params=`json=${json}`;
    let headers=new Headers({'Content-Type':'application/x-www-form-urlencoded'});

    return this.http.post(this.url+"products",params,{headers:headers})
    .map(res=>res.json());
  };
  editProduct=(product:Product,id:number)=>{
    let json:string=JSON.stringify(product);
    let params=`json=${json}`;
    let headers=new Headers({'Content-Type':'application/x-www-form-urlencoded'});

    return this.http.post(this.url+"update-product/"+id,params,{headers:headers})
    .map(res=>res.json());
  };
  deleteProduct=(id:number)=>{
    return this.http.get(this.url+`delete-product/${id}`)
    .map(res=>res.json());
  };
}
