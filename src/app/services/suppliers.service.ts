import { Injectable } from '@angular/core';
import {GLOBAL} from './global';
import {Supplier} from '../models/supplier.model';
import {Http,Headers} from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class SuppliersService {
  private url:string;
  constructor(private http:Http) {
    this.url=GLOBAL.url;
  }
  getSuppliers=()=>{
    return this.http.get(this.url+"suppliers")
    .map(res=>res.json());
  };
  getSupplier=(id:number)=>{
    return this.http.get(this.url+`supplier/${id}`)
    .map(res=>res.json());
  };
  addSupplier=(supplier:Supplier)=>{
    let json:string=JSON.stringify(supplier);
    let params=`json=${json}`;
    let headers=new Headers({'Content-Type':'application/x-www-form-urlencoded'});

    return this.http.post(this.url+"suppliers",params,{headers:headers})
    .map(res=>res.json());
  };
  editSupplier=(supplier:Supplier,id:number)=>{
    let json:string=JSON.stringify(supplier);
    let params=`json=${json}`;
    let headers=new Headers({'Content-Type':'application/x-www-form-urlencoded'});

    return this.http.post(this.url+"update-supplier/"+id,params,{headers:headers})
    .map(res=>res.json());
  };
  deleteSupplier=(id:number)=>{
    return this.http.get(this.url+`delete-supplier/${id}`)
    .map(res=>res.json());
  };

}
