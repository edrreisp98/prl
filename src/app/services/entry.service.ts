import { Injectable } from '@angular/core';
import {GLOBAL} from './global';
import {Entry} from '../models/entry.model';
import {Http,Headers} from '@angular/http';
@Injectable()
export class EntryService {

  private url:string;
  constructor(private http:Http) {
    this.url=GLOBAL.url;
  }

  getEntries=()=>{
    return this.http.get(this.url+"entries")
    .map(res=>res.json());
  }
  cancelEntry=(entry:Entry)=>{
    let json:string=JSON.stringify(entry);
    let params=`json=${json}`;
    let headers=new Headers({'Content-Type':'application/x-www-form-urlencoded'});

    return this.http.post(this.url+"entry-cancel",params,{headers:headers})
    .map(res=>res.json());
  }

  addEntry=(entry:Entry)=>{
    let json:string=JSON.stringify(entry);
    let params=`json=${json}`
    let headers=new Headers({'Content-Type':'application/x-www-form-urlencoded'});

    return this.http.post(this.url+"entries",params,{headers:headers})
    .map(res=>res.json());
  }
}
