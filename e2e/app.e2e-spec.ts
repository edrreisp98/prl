import { PrlPage } from './app.po';

describe('prl App', () => {
  let page: PrlPage;

  beforeEach(() => {
    page = new PrlPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
